
# For Xelect in-house use of Angus with Xellotape
______________________________

## Introduction

Xellotape is an R package used internally in Xelect and is already installed in Gandalf, with a collection of R functions that can be used in association with Angus.
Xellotape contains R functions for MiSeq runs, as well as fluidigm runs, but here we will focus only to the functions that are related to the Angus workflow and the MiSeq.

The R funtions that can be found in Xellotape are:

1. create_run_series()

2. add_htas_run()

3. evaluate_angus()

4. build_htas_genotypes()


!!! important
	There is an R markdown template, which can be found in Rstudio server (File -> New File -> R Markdown -> From Template -> Angus Workflow Template), that contains all the steps using the above R functions from Xellotape.



### 1. Set the paths and names that will be used for Angus 


	>   dr <- "/home/htas/jobs/2020_htas_jobs/"  # The path where you want to create the job directory
		rn <- "xj20004f_run01" # The name of the run in basespace 
		sn <- "xj20004f" # Series name. This can be the original job cobe without the extension "run01", "run02", etc.
		jn <- "xj20004f_run01" # The name of the job or jobs (if two jobs in one run then add both names separated by comma and no spaces (eg "xj19075,xj19076")
		bp <- "~/BaseSpace_Mount/" # Path of BaseSpace
		pn <- "ssal_wfgs_na_v5" # The name of the panel that you want to use as in /home/htas/panels
		pn2 <- "" # The name of the second panel (if necessary)


Where: <br />
	&emsp;&emsp; **- dr:** The name you want to give to the directory which will store the Angus run <br />
	&emsp;&emsp; **- rn:** The name of the jobs included in the run <br />
	&emsp;&emsp; **- sn:** The path where you want to create the structure of files <br />
	&emsp;&emsp; **- jn:** The name of the jobs included in the run <br />
	&emsp;&emsp; **- bp:** The path where you want to create the structure of files <br />
	&emsp;&emsp; **- pn:** The name of the jobs included in the run <br />
	&emsp;&emsp; **- pn2:** The path where you want to create the structure of files



This R function creates the structure of folders needed for the input and output files required for Angus. It is a mandatory step before starting using Angus, since all the relative paths must be created before hand. 
To run **create_run_series** through RStudio type in console:


	> xellotape::create_run_series(
	series_name = "xj20000_xj20001",
	jobs = "xj20000,xj20001",
	dir = "name_of_directory")

Where: <br />
	&emsp;&emsp; **- series_name:** The name you want to give to the directory which will store the Angus run <br />
	&emsp;&emsp; **- jobs:** The name of the jobs included in the run <br />
	&emsp;&emsp; **- dir:** The path where you want to create the structure of files

### 2. add_htas_run()
The next step is to run **add_htas_run** function which copies a clean version of the Angus workflow in the directory that was created in 'step 1' and then it runs *build_config.py* script from Angus to create the *config.yaml* file which contains all the necessary configurations for the genotyping workflow.

!!! important ""

	There are four different methods for running add_htas_run(), based on whether there is a panel developed for the species we want to run or not and based on whether the samples in the run are from one species or two (two different panels. 

To run **add_htas_run** through RStudio type in console:

** a. One panel - one job**

	> xellotape::add_htas_run(
	dir = "dir_path",
	run_name = "basespace_run_name",
	panel_name = "name_of_the_panel",
	sample_sheet = "path_of_samplesheet_in_basespace", 
	illumina_dir = "Basespace_path", 
	copy_raw = "Yes")

Where: <br />
	&emsp;&emsp; **- dir:** The relative path of the directory you created earlier with the structure of files <br />
	&emsp;&emsp; **- run_name:** The name of the job as it is stored in Basespace <br />
	&emsp;&emsp; **- panel_name:** The name of the panel you want ot use from "/htas/panels/" <br />
	&emsp;&emsp; **- sample_sheet:** The full path of the location where the samplesheet is located <br />
	&emsp;&emsp; **- illumina_dir:** The full path of BaseSpace <br />
	&emsp;&emsp; **- copy_raw:** Copy all raw fastq files from Basespace into the working directory (Must be Yes when working with Basespace)


** b. No panel - one job**

!!! important ""
	Paths of loadings, targets and Fasta file should be added manually

	> add_htas_run( dir = file.path(dr, sn), #The complete path of the directory where the folder structure is for this job
		run_name = rn,
		loadings = "path_loadings_file",
		targets =  "path_to_targets_file",
		master_fasta = "path_to_fasta",
		sample_sheet = file.path(bp, "Runs", rn, "Files", "SampleSheet.csv"), # The path where the SampleSheet is (usually in ~/BaseSpace/Runs/)
		illumina_dir = bp,
		copy_raw = "Yes")



### 3. run_angus()

This step can be performed by either using Rstudio console or the terminal when connected to Gandalf (Xelect's in-house server). 
This R function runs Snakemake, the workflow manager that Angus is built on, and it starts a number of processing steps which will produce genotypes from the MiSeq.

**Rstudio method**

	> csh <- "/bin/Anaconda/etc/profile.d/conda.sh"
	> setwd(file.path("dir", "series_name", "runs", "job_name", "angus"))
	> xelectseq::condasys("snakemake --cores 4", env = "ngs_env", conda_sh_path = csh)

**Linux method** (Preferable)

	$ cd /path/where/Angus/dir/is/ 
	$ conda activate ngs_env 
	$ snakemake --cores 4
