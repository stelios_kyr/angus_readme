# Installation


## Git Installation on Linux
______________________________


To clone Angus straight from its Bitbucket repository you will need to have installed Git in your computer if not already installed.

If you are using Fedora (or any closely-related RPM-based distribution, such as RHEL or CentOS), you can use `dnf`:


	$ sudo dnf install git-all


If you're on a Debian-based distribution, such as Ubuntu, try `apt`:


	$ sudo apt install git-all


For more options, there are instructions for installing on several different Unix distributions on the Git website, at [https://git-scm.com/download/linux](https://git-scm.com/download/linux).


## Downloading Angus
______________________________


"Angus" can be downloaded and installed from a [Bitbucket repository] or by cloning the repository using `git clone`.

**Embric version:**


	$ git clone git@bitbucket.org:chollenbeck/angus.git


**Xelect version:**


	$ git clone git@bitbucket.org:xelect_ltd/angus.git


As mentioned before, Angus has many dependencies and it is suggested to use one of the [Anaconda](https://docs.anaconda.com/anaconda/install/) or [Miniconda](https://docs.conda.io/en/latest/miniconda.html) distributions to create an environment using the provided yaml file in Angus and avoiding compiling dependencies one at a time which is time consuming.

To do that the following steps can be followed:


## Downloading and Installing (Ana|mini)conda:
______________________________


**You will only need to donwload and install one conda installer, Anaconda or Miniconda.**

!!! note
	Before you install (Ana|mini)conda, Curl is needed so if not installed you can install it 
	by typing:
	
		$ sudo apt install curl

!!! hint
	You can find installation instuctions for both Anaconda and Miniconda here:

	* [Anaconda](https://docs.anaconda.com/anaconda/install/) - Free

	* [Miniconda](https://docs.conda.io/en/latest/miniconda.html) - Free



### a. Anaconda installation


There are two options for installing Anaconda:

**1. Using curl to download and install Anaconda.**

To do that just type in the command line: 

	$ curl -o /tmp/Anaconda.sh
	https://repo.continuum.io/archive/Anaconda3-5.1.0-Linux-x86_64.sh && bash /tmp/Anaconda.sh 

**2. or download the available installer version of [Anaconda](https://docs.anaconda.com/anaconda/install/)**



After having downloaded Anaconda by using either of the two above methods, change directory to where the installer is and type:


	$ cd /tmp/Anaconda.sh

	$ bash Anaconda.sh


### b. Miniconda installation


Miniconda can be installed using the same two methods described for Anaconda.

**1. Using curl to download and install Miniconda3.**

To do that just type in the command line:


	$ curl -o /tmp/miniconda.sh 
	https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh && bash /tmp/miniconda.sh


**2. Download the available installer version of [Miniconda](https://docs.conda.io/en/latest/miniconda.html)**

Then change directory to where the installer is:


	$ cd ~/Path/to/Miniconda3.sh

Run the installer by typing in terminal:

 
	$ bash Miniconda3-latest-Linux-x86_64.sh


!!! note
	Answer yes to the question whether conda shall be put into your PATH.

!!! note
	You must restart your account for the changes to take place or run ~/.bashrc from your terminal.


## Creating an environment
______________________________

 For using Angus it is suggested to create an environment dedicated to Angus. This environment is a directory that contains all dependencies for Angus and it can be activated before running the workflow and deactivated after it finishes. The yaml file containing all environment dependencies for Angus is located in **/bin/install/environment.yml** and can be created as follows:

**Installing available environment**

	$ conda env create
	--name *environment_name
	--file bin/install/environment.yml

**Activating the environment**

	$ conda activate *environment_name*


**and deactivate it by using**


	$ conda deactivate


!!! note
	After creating the conda environment, an additional R package is needed for the seamless function of Angus. The R package [microhaplot](https://github.com/ngthomas/microhaplot) needs to be installed in R or Rstudio using the above commands
	> library(devtools)
	or
	> install.packages("devtools")
	if devtools is not installed in your machine
	and then 
	> devtools::install_github("ngthomas/microhaplot", build_vignettes = TRUE, build_opts = c("--no-resave-data", "--no-manual"))


!! note
	Also you might need to install libfretype6-dev by using
	$ sudo apt-get install libfreetype6-dev
	from shell
	if you get an error that this package cannot be found when installing microhaplot
	then
	$ sudo apt-get install libfontconfig-dev
	and
	$ sudo apt-get install libcairo2-dev




## Quick start
______________________________


For running Angus with default parameters, the following set of commands must be executed.

1)  Change home directory where Angus is downloaded:

	$ cd /home/*Angus*/ 


2) Run python script **build_config.py** which creates the config.yaml file containing all the run parameters and paths of the raw fastq files.

	$ python build_config.py 
	-g data/genome/*sequence.fa*
	-d /home/Path/of/Raw_Fastq


3) Run snakemake for Angus

	$ snakemake --h [optional parameters]


