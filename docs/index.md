# Angus

## Introduction
____________________________

Angus is a customizable workflow for processing Next Generation Sequencing (NGS) data, designed and implemented using the workflow manager [Snakemake](https://snakemake.readthedocs.io/en/stable/).
The functionality of Angus include all appropriate steps for read preprocessing, mapping reads to a reference sequence and variant calling to generate a list of variants Single Nucleotide Polymorphisms (SNPs) using [FreeBayes](https://github.com/ekg/freebayes). Additionally, it consists of a processing step where by using the identified variants it can produce a set of haplotypes using the tools [microhaplot](https://github.com/ngthomas/microhaplot.git) and [rad_haplotyper](https://github.com/chollenbeck/rad_haplotyper). Rad_haplotyper is a program designed to produce SNP haplotypes from RAD-seq data with fixed-size RAD loci (either single- or paired-end double digest RAD sequences or single-end, single-digest RAD sequences).

Angus is designed to run on Linux systems and requires a set of software tools and packages to be isntalled. All the required tools can be found in the form of a list in a single configuration file named `env.yaml` which is included inside the Angus folder and is ready to be installed using [Conda](https://docs.conda.io/projects/conda/en/latest/index.html). Conda is an open source package and environment management system for installing multiple versions of software packages and their dependencies and switching easily between them. It works on Linux, OS X and Windows, and was created for Python programs but can package and distribute any software.

The Angus workflow is designed to handle both single-end and paired-end sequencing data, by detecting automatically the type of reads, and it gives the user the flexibility to filter the identified SNPs, as well as all the other parameters required for mapping, trimming, and variant calling variant. Then the variants can be used for haplotype calling if required, or an existing list of known variants (target file) can be provided from the beginning so the workflow can call only these if possible and use them for haplotype calling.


## Overview
____________________________

Typical Angus workflow consists of 9 basic processing steps:

1. Quality control of NGS raw data using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) (Optional) 
2. Quality trimming and adapter removal with [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic)
3. Mapping of the reads using [BWA-mem](http://bio-bwa.sourceforge.net/bwa.shtml)
4. Marking of duplicated reads (Removal of duplicates is optional) using [SAMtools](http://www.htslib.org/doc/samtools.html)
5. Variant calling using [FreeBayes](https://github.com/ekg/freebayes) 
6. Variant filtering using [VCFtools](http://vcftools.sourceforge.net/man_latest.html)
7. Haplotype calling using [rad_haplotyper](https://github.com/chollenbeck/rad_haplotyper) 
8. Generating visual summaries of microhaplotypes found in short read alignments using [microhaplot](https://github.com/ngthomas/microhaplot)
9. Report files such as mapping summary statistics, call rates and coverage statistics for loci and individuals.

